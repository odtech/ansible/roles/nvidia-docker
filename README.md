# NVIDIA Docker

Installs the NVIDIA Docker runtime.

## Example Usage
### Playbook.yml
```yaml
- hosts: all
  gather_facts: true
  roles:
    - role: nvidia-docker
```
### Variables
```yaml
nvidia_docker_version: "2.0.3"
nvidia_container_runtime_version: "2.0.0"
nvidia_docker_docker_version: "18.09.6"
```
### Requirements.yml
```yaml
- src: https://gitlab.com/odtech/ansible/roles/nvidia-docker.git
  scm: git
  version: master
```
## Authors

* **Andrew Strangwood** - [ODTech](https://gitlab.com/odtech)

See also the list of [contributors](CONTRIBUTING.md) who participated in this project.

## License

This project is licensed under the BSD-3-Clause License - see the [LICENSE](LICENSE) file for details

## Links 

[NVIDIA Docker Runtime](https://github.com/NVIDIA/nvidia-docker)

[NVIDIA Docker Repository](https://nvidia.github.io/nvidia-docker/)

[Docker Releases](https://download.docker.com/linux/ubuntu/dists/)